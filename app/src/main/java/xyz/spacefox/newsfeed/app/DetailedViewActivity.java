package xyz.spacefox.newsfeed.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import xyz.spacefox.newsfeed.app.utils.Consts;

/**
 * Detailed view activity: shows news in WebView in case when application runs in Single-pane mode..
 */
public class DetailedViewActivity extends AppCompatActivity {

    // News url to display
    private String mNewsUrl;

    // Fragment that displays webpage
    private DetailedViewFragment mDetailedViewFragment;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_view);

        // Init the toolbar
        initToolbar();

        // Getting Url to display from intent extra
        mNewsUrl = getIntent().getExtras().getString(Consts.INTENT_URL_EXTRA, "");

        // If application in single-pane mode - close the activity
        if (getResources().getBoolean(R.bool.has_two_panes)) {
            finish();
        }

        // Find Fragment and display news webpage
        mDetailedViewFragment = (DetailedViewFragment) getFragmentManager()
                .findFragmentById(R.id.news_view_fragment_holder);
        if (mDetailedViewFragment == null) {
            mDetailedViewFragment = DetailedViewFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.news_view_fragment_holder, mDetailedViewFragment).commit();
        }

        mDetailedViewFragment.displayNews(mNewsUrl);

    }

    /** Initalizes the toolbar. */
    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
