package xyz.spacefox.newsfeed.app.api;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import xyz.spacefox.newsfeed.app.utils.Consts;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Deserializer class for GSON to properly deserialize date given from API to standard Java
 * {@link java.util.Date Date} object
 */
public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String date = element.getAsString();
        date = date.substring(0, date.length()-4);

        SimpleDateFormat format = new SimpleDateFormat(Consts.API_DATE_FORMAT, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone(Consts.API_DATE_TIMEZONE));

        try {
            return format.parse(date);
        } catch (ParseException exp) {
            return null;
        }
    }
}
