package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;
import xyz.spacefox.newsfeed.app.utils.Consts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Class that represent News item JSON object from news API
 */
public class NewsItem {

    @SerializedName("NewsItemId")
    private int mNewsItemId;

    @SerializedName("HeadLine")
    private String mHeadLine;

    @SerializedName("Agency")
    private String mAgency;

    @SerializedName("DateLine")
    private Date mNewsDate;

    @SerializedName("WebURL")
    private String mWebUrl;

    @SerializedName("Caption")
    private String mCaption;

    @SerializedName("Image")
    private NewsImage mNewsImage;

    @SerializedName("Keywords")
    private String mKeywords;

    @SerializedName("Story")
    private String mStory;

    @SerializedName("HomeRelated")
    private List<NewsHomeRelated> mHomeRelated;

    @SerializedName("CommentCountUrl")
    private String mCommentCountUrl;

    @SerializedName("CommentFeedUrl")
    private String mCommectFeedUrl;

    @SerializedName("Related")
    private String mRelatedUrl;

    @SerializedName("Video")
    private List<NewsVideo> mNewsVideos;

    public int getNewsItemId() {
        return mNewsItemId;
    }

    public String getHeadLine() {
        return mHeadLine;
    }

    public String getAgency() {
        return mAgency;
    }

    public Date getNewsDate() {
        return mNewsDate;
    }

    public String getWebUrl() {
        return mWebUrl;
    }

    public String getCaption() {
        return mCaption;
    }

    public NewsImage getNewsImage() {
        return mNewsImage;
    }

    public String getKeywords() {
        return mKeywords;
    }

    public String getStory() {
        return mStory;
    }

    public List<NewsHomeRelated> getHomeRelated() {
        return mHomeRelated;
    }

    public String getCommentCountUrl() {
        return mCommentCountUrl;
    }

    public String getCommectFeedUrl() {
        return mCommectFeedUrl;
    }

    public String getRelatedUrl() {
        return mRelatedUrl;
    }

    public List<NewsVideo> getNewsVideos() {
        return mNewsVideos;
    }

    public String getNewsDateString(){
        SimpleDateFormat formatter = new SimpleDateFormat(Consts.DISPLAY_DATE_FORMAT, Locale.ENGLISH);
        return formatter.format(getNewsDate());
    }

    public void setNewsItemId(int mNewsItemId) {
        this.mNewsItemId = mNewsItemId;
    }

    public void setHeadLine(String mHeadLine) {
        this.mHeadLine = mHeadLine;
    }

    public void setAgency(String mAgency) {
        this.mAgency = mAgency;
    }

    public void setNewsDate(Date mNewsDate) {
        this.mNewsDate = mNewsDate;
    }

    public void setWebUrl(String mWebUrl) {
        this.mWebUrl = mWebUrl;
    }

    public void setCaption(String mCaption) {
        this.mCaption = mCaption;
    }

    public void setNewsImage(NewsImage mNewsImage) {
        this.mNewsImage = mNewsImage;
    }

    public void setKeywords(String mKeywords) {
        this.mKeywords = mKeywords;
    }

    public void setStory(String mStory) {
        this.mStory = mStory;
    }

    public void setHomeRelated(List<NewsHomeRelated> mHomeRelated) {
        this.mHomeRelated = mHomeRelated;
    }

    public void setCommentCountUrl(String mCommentCountUrl) {
        this.mCommentCountUrl = mCommentCountUrl;
    }

    public void setCommectFeedUrl(String mCommectFeedUrl) {
        this.mCommectFeedUrl = mCommectFeedUrl;
    }

    public void setRelatedUrl(String mRelatedUrl) {
        this.mRelatedUrl = mRelatedUrl;
    }

    public void setNewsVideos(List<NewsVideo> mNewsVideos) {
        this.mNewsVideos = mNewsVideos;
    }

    @Override
    public int hashCode() {
        return getNewsItemId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        NewsItem other = (NewsItem) o;
        if (mNewsItemId != other.getNewsItemId())
            return false;
        return true;
    }
}
