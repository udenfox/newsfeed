package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represent HomeRelated item JSON object from news API
 */
public class NewsHomeRelated {

    @SerializedName("NewsItemId")
    private int mNewsItemId;

    @SerializedName("HeadLine")
    private String mHeadLine;

    @SerializedName("DetailFeed")
    private String mDetailFeedUrl;

    @SerializedName("Type")
    private String mType;

    public int getNewsItemId() {
        return mNewsItemId;
    }

    public String getHeadLine() {
        return mHeadLine;
    }

    public String getDetailFeedUrl() {
        return mDetailFeedUrl;
    }

    public String getType() {
        return mType;
    }

    @Override
    public int hashCode() {
        return getNewsItemId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        NewsHomeRelated other = (NewsHomeRelated) o;
        if (mNewsItemId != other.getNewsItemId())
            return false;
        return true;
    }
}
