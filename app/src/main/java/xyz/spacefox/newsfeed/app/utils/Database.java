package xyz.spacefox.newsfeed.app.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import xyz.spacefox.newsfeed.app.api.model.NewsItem;

import java.util.List;

/**
 * Class to simplify interaction with the database. Contains specific methods to interact
 * with databese depends on project's needs.
 */
public class Database {

    private final Context mCtx;

    // Custom DbHelper.
    private DBHelper mDBHelper;

    // Database itself
    private SQLiteDatabase mDB;

    /**
     * Constructor of database object.
     * @param ctx Context for database.
     */
    public Database(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Opens a connection to database
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, Consts.DB_NAME, null, Consts.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closes the connection to database.
     */
    public void close() {
        if (mDBHelper!=null) mDBHelper.close();
    }

    /**
     * Returns cursor with all data from database
     */
    public Cursor getAllData() {
        return mDB.query(Consts.DB_TABLE_NAME, null, null, null, null, null, Consts.DB_COL_ID_PRIMARY +" DESC");
    }

    /**
     * Add news item to the database
     */
    private void addRec(NewsItem item) {
        ContentValues cv = new ContentValues();
        cv.put(Consts.DB_COL_ID, item.getNewsItemId());
        cv.put(Consts.DB_COL_CAPTION, item.getCaption());
        cv.put(Consts.DB_COL_HEADER, item.getHeadLine());
        cv.put(Consts.DB_COL_AGENCY, item.getAgency());
        cv.put(Consts.DB_COL_URL, item.getWebUrl());
        cv.put(Consts.DB_COL_IMAGE, item.getNewsImage().getImageThumbUrl());
        cv.put(Consts.DB_COL_DATE, item.getNewsDate().getTime());

        mDB.insert(Consts.DB_TABLE_NAME, null, cv);
    }

    /**
     * Add list of news items to the database
     */
    public void addApiData(List<NewsItem> newsItems){
        if (newsItems.size() != 0) {
            for (int i = newsItems.size()-1; i>=0; i--){
                addRec(newsItems.get(i));
            }
        }

    }

    /**
     * Delete all rows from database except last number of rows.
     * @param itemsToRetain number of rows to save in database.
     */
    public void deleteOver(int itemsToRetain) {
        mDB.delete(Consts.DB_TABLE_NAME,
                Consts.DB_COL_ID + " not in " +
                        "(select "+Consts.DB_COL_ID+" from " + Consts.DB_TABLE_NAME +
                        " order by "+Consts.DB_COL_ID_PRIMARY +" desc limit "+ String.valueOf(itemsToRetain) +
                        ")", null);
    }

    /**
     * Subclass of {@link android.database.sqlite.SQLiteOpenHelper} which provides custom database helper.
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Consts.DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(Consts.DB_DELETE_ENTRIES);
            onCreate(db);
        }
    }

}
