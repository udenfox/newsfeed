package xyz.spacefox.newsfeed.app.api;

import retrofit.Callback;
import retrofit.http.GET;
import xyz.spacefox.newsfeed.app.api.model.ApiResponse;

/**
 * Interface to do API requests using retrofit
 */
public interface ApiService {

    @GET("/newsdefaultfeeds.cms?feedtype=sjson")
    public void getNews(Callback<ApiResponse> callback);
}
