package xyz.spacefox.newsfeed.app.utils;

/**
 * Class that contains all project constants
 */
public class Consts {

    // Url to main API
    public final static String API_URL = "http://timesofindia.indiatimes.com/feeds";

    // Data for deserialize and display date format
    public final static String API_DATE_FORMAT  = "MMM dd, yyyy, hh.mma";
    public final static String DISPLAY_DATE_FORMAT  = "MMM dd, yyyy, hh.mma";
    public final static String API_DATE_TIMEZONE = "Asia/Calcutta";

    // Names of intents/preferences
    public final static String INDEX_STATE_NAME = "itemIndex";
    public final static String FEED_LIST_STATE_NAME = "listState";
    public final static String INTENT_URL_EXTRA = "newsUrl";

    // Configuration of a CursorLoader
    public final static int LOADER_ID = 0;
    public final static int MAX_NEWS = 25;

    // Configuration of a database
    public final static String DB_NAME = "nf_db";
    public final static String DB_TABLE_NAME = "nf_news";
    public final static String DB_COL_ID_PRIMARY = "_id";
    public final static String DB_COL_ID = "newsId";
    public final static String DB_COL_HEADER = "header";
    public final static String DB_COL_CAPTION = "caption";
    public final static String DB_COL_IMAGE = "image_url";
    public final static String DB_COL_URL = "news_url";
    public final static String DB_COL_AGENCY = "agency";
    public final static String DB_COL_DATE = "news_date";
    public final static int DB_VERSION = 3;

    // SQL Query
    public static final String DB_CREATE =
            "create table " + DB_TABLE_NAME + "(" +
                    DB_COL_ID_PRIMARY + " integer primary key autoincrement, " +
                    DB_COL_ID + " integer, " +
                    DB_COL_HEADER + " text, " +
                    DB_COL_URL + " text," +
                    DB_COL_CAPTION + " text," +
                    DB_COL_DATE + " integer," +
                    DB_COL_IMAGE + " text," +
                    DB_COL_AGENCY + " text," +
                    " UNIQUE ( "+DB_COL_ID+" ) ON CONFLICT IGNORE"+
                    ");";

    public static final String DB_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DB_TABLE_NAME;


}
