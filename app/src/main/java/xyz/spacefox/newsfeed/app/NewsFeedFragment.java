package xyz.spacefox.newsfeed.app;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import xyz.spacefox.newsfeed.app.api.RestClient;
import xyz.spacefox.newsfeed.app.api.model.ApiResponse;
import xyz.spacefox.newsfeed.app.ui.NfCursorAdapter;
import xyz.spacefox.newsfeed.app.ui.SwipeRefreshListFragment;
import xyz.spacefox.newsfeed.app.utils.Consts;
import xyz.spacefox.newsfeed.app.utils.Database;

/**
 * Fragment that displays the news feed.
 *
 * This Fragment displays a list with the news headlines and picture preview.
 * When an item is selected, it notifies the listener that an item was selected.
 *
 * Implements LoaderCallbacks to get connection with database.
 */
public class NewsFeedFragment extends SwipeRefreshListFragment implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    // Adapter for news feed Listview
    private NfCursorAdapter mNewsCursorAdapter;

    // List state bundle. Used for recover list position after fragment is resumed
    private Parcelable mListState;

    //Listener for notify when news item selected from the list
    private OnNewsItemSelectedListener mNewsItemSelectedListener = null;

    // Database to connect to
    private Database mNewsDb;

    // Flag to set select mode to a ListView
    private boolean mSelectableMode;

    // Flag to check is this a first data load
    private Boolean mIsFirstLoaded = true;

    private int mSelectedItem = -1;

    /**
     * Represents a listener that will be notify about news item selections.
     */
    public interface OnNewsItemSelectedListener {
        /**
         * Called when a news item is selected.
         * @param url the url of selecter news.
         * @param position the index of selected item.
         */
        public void onNewsItemSelected(String url, int position);
    }

    /**
     * Create a new instance of NewsFeedFragment.
     */
    public static NewsFeedFragment newInstance(){
        NewsFeedFragment f = new NewsFeedFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNewsDb = new Database(getActivity());
        mNewsCursorAdapter = new NfCursorAdapter(getActivity(), null, true);

        // Open our database
        mNewsDb.open();

        // Initialize cursor loader
        getLoaderManager().initLoader(Consts.LOADER_ID, null, this);

        // Load news only if this is first fragment start
        if(savedInstanceState == null) {
            loadNews();
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        // Init the ListView
        getListView().setDividerHeight((int)getResources().getDimension(R.dimen.nf_divider_height));
        setListAdapter(mNewsCursorAdapter);
        getListView().setOnItemClickListener(this);
        setSelectMode(mSelectableMode);

        // Setsing color theme for swipe-refresh
        setColorScheme(R.color.accent);

        // Set Refresh listener for swipe-refresh
        setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNews();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        // Resume List state if saved after fragment resuming and if this is not first data load.
        if (mListState!=null) {
            getListView().onRestoreInstanceState(mListState);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Destroy CursorLoader and connection to database to avoid crashes.
        getLoaderManager().destroyLoader(Consts.LOADER_ID);
        mNewsDb.close();

    }

    /**
     * Returns current list state. Also refresh current list state variable.
     */
    public Parcelable getListState(){
        mListState = getListView().onSaveInstanceState();
        return mListState;
    }

    /**
     * Sets list state information to the local variable for list state resume
     * @param listState List saved state
     */
    public void setListState(Parcelable listState) {
        mListState = listState;
    }

    /**
     * Handles a click on a news item
     *
     * This causes the configured listener to be notified that item was selected.
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (mNewsItemSelectedListener != null) {
            mNewsItemSelectedListener.onNewsItemSelected(mNewsCursorAdapter.getItem(position).getWebUrl(), position);
        }
    }

    /**
     * Sets the listener that should be notified when news item selected.
     * @param listener the listener to notify.
     */
    public void setOnNewsItemSelectedListener(OnNewsItemSelectedListener listener) {
        mNewsItemSelectedListener = listener;
    }

    /** Sets choice mode to apply for the list
     * @param selectable sets is list selectable.
     */
    public void setSelectable(boolean selectable) {
        mSelectableMode = selectable;
    }


    /** Applys choice mode for the list
     * @param isSelectable sets is list selectable.
     */
    private void setSelectMode (boolean isSelectable){
        if (isSelectable) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
        else {
            getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
        }
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);
        if (mNewsItemSelectedListener != null  && mSelectableMode) {
            // Check selected item just like user clecked on it and notify about click
            getListView().setItemChecked(position, true);
            mNewsItemSelectedListener.onNewsItemSelected(mNewsCursorAdapter.getItem(position).getWebUrl(), position);
        }
    }

    /**
     * Set selected item to restore after listView created
     * @param position selected item position
     */
    public void setSelectedItem(int position){
        mSelectedItem = position;
    }

    /** Loads the news using API */
    private void loadNews(){

        // Try to load cached data, if available
        if (getLoaderManager().getLoader(Consts.LOADER_ID) != null) {
            getLoaderManager().getLoader(Consts.LOADER_ID).forceLoad();
        }

        // Try to load entries from API
        RestClient.getInstance().getApiService().getNews(new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                // Add getted data to db.
                mNewsDb.addApiData(apiResponse.getNewsItems());
                // Save only specified numbers of rows in database
                mNewsDb.deleteOver(Consts.MAX_NEWS);
                // Reload cursor
                getLoaderManager().getLoader(Consts.LOADER_ID).forceLoad();
                setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), getString(R.string.network_error_toast), Toast.LENGTH_SHORT).show();
                setRefreshing(false);
            }
        });


    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new MyCursorLoader(getActivity(), mNewsDb);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Set cursor to out adapter
        mNewsCursorAdapter.swapCursor(cursor);

        // Resume list after loader is created to avoid early state resume.
        if (mIsFirstLoaded) {
            if (mListState!=null) {
                getListView().onRestoreInstanceState(mListState);

                // Set first loaded check to false to avoid resuming state on every list refresh.
                mIsFirstLoaded = false;
            }

            // Set selection to saved state item.
            if (mSelectedItem>=0) {
                setSelection(mSelectedItem);
            }
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    /**
     * Subclass of {@link android.content.CursorLoader} which provides loader associated
     * with application database's implementation.
     */
    static class MyCursorLoader extends CursorLoader {

        Database db;

        public MyCursorLoader(Context context, Database db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            return db.getAllData();
        }

    }


}
