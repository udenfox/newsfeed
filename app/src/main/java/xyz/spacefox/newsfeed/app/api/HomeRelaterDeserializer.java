package xyz.spacefox.newsfeed.app.api;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import xyz.spacefox.newsfeed.app.api.model.NewsHomeRelated;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Deserializer class for GSON to properly deserialize home related JSON object given from API.
 * Checks if home related object from JSON is array or single object and returns
 * {@link NewsHomeRelated HomeRelated} objects list in anyway.
 */
public class HomeRelaterDeserializer implements JsonDeserializer<List<NewsHomeRelated>>{

    @Override
    public List<NewsHomeRelated> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<NewsHomeRelated> newsHomeRelateds = new ArrayList<NewsHomeRelated>();

        if (json.isJsonArray()) {
            for (JsonElement e : json.getAsJsonArray()) {
                newsHomeRelateds.add((NewsHomeRelated) context.deserialize(e, NewsHomeRelated.class));
            }
        } else if (json.isJsonObject()) {
            newsHomeRelateds.add((NewsHomeRelated) context.deserialize(json, NewsHomeRelated.class));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + json.getClass());
        }

        return newsHomeRelateds;
    }
}
