package xyz.spacefox.newsfeed.app.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import xyz.spacefox.newsfeed.app.R;
import xyz.spacefox.newsfeed.app.api.model.NewsItem;
import xyz.spacefox.newsfeed.app.utils.Consts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides cursor adapter for custom list item and dataset.
 */
public class NfCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;

    // Formatter to format Date object getted from database
    private SimpleDateFormat mFormatter;

    public NfCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        // Setting pattern for the formatter
        mFormatter = new SimpleDateFormat(Consts.DISPLAY_DATE_FORMAT, Locale.ENGLISH);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return cursorInflater.inflate(R.layout.nf_item, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Getting item view elements
        TextView newsHeader = (TextView) view.findViewById(R.id.tv_nf_item_title);
        TextView newsText = (TextView) view.findViewById(R.id.tv_nf_item_main);
        TextView newsAdditional = (TextView) view.findViewById(R.id.tv_nf_item_additional);
        ImageView newsPicture = (ImageView) view.findViewById(R.id.iv_nf_item_picture);

        // Getting news date from database and convert it to Date object
        Date newsDate = new Date(cursor.getLong(cursor.getColumnIndex(Consts.DB_COL_DATE)));

        // Getting and combining additional news data (agency and date)
        String agency = cursor.getString(cursor.getColumnIndex(Consts.DB_COL_AGENCY));
        String date = mFormatter.format(newsDate);
        StringBuilder additionalBuf = new StringBuilder();
        if(agency!=null){
            additionalBuf.append(agency).append(",   ");
        }
        additionalBuf.append(date);

        // Setting data to a views
        newsHeader.setText(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_HEADER)));
        newsText.setText(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_CAPTION)));
        newsHeader.setText(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_HEADER)));
        newsAdditional.setText(additionalBuf.toString());
        Picasso.with(context).load(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_IMAGE)))
                .placeholder(R.drawable.image_placeholder)
                .into(newsPicture);
    }

    /**
     * Returns an {@link xyz.spacefox.newsfeed.app.api.model.NewsItem} object colected
     * from database at specified position.
     * @param position Position of item to get
     */
    public NewsItem getItem (int position) {
        Cursor cursor = getCursor();
        NewsItem item = new NewsItem();
        if(cursor.moveToPosition(position)) {
            item = new NewsItem();
            item.setAgency(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_AGENCY)));
            item.setCaption(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_CAPTION)));
            item.setHeadLine(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_HEADER)));
            item.setNewsDate(new Date(cursor.getInt(cursor.getColumnIndex(Consts.DB_COL_DATE))));
            item.setWebUrl(cursor.getString(cursor.getColumnIndex(Consts.DB_COL_URL)));
            item.setNewsItemId(cursor.getInt(cursor.getColumnIndex(Consts.DB_COL_ID)));
        }

        return item;
    }
}
