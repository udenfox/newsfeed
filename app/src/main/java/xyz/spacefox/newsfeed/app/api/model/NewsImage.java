package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represent News picture JSON object from news API
 */
public class NewsImage {

    @SerializedName("Photo")
    private String mImageUrl;

    @SerializedName("Thumb")
    private String mImageThumbUrl;

    @SerializedName("PhotoCaption")
    private String mImageCaption;


    public String getImageUrl() {
        return mImageUrl;
    }

    public String getImageThumbUrl() {
        return mImageThumbUrl;
    }

    public String getImageCaption() {
        return mImageCaption;
    }

    @Override
    public int hashCode() {
        String toComp = mImageUrl + mImageThumbUrl;
        return toComp.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        NewsImage other = (NewsImage) o;
        if (!mImageUrl.equals(other.getImageUrl()) || !mImageThumbUrl.equals(other.getImageThumbUrl()))
            return false;
        return true;
    }
}
