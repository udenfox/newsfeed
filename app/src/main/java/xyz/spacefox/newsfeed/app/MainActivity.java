package xyz.spacefox.newsfeed.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import xyz.spacefox.newsfeed.app.utils.Consts;

/**
 * Main activity: shows news feed news itself, if layout permits.
 *
 * Implements two activity mode depends on device screen: single-pane mode and dual-pane mode
 *
 * In single-pane mode, this activity shows a news feed provided by {@link NewsFeedFragment}.
 * When the user clicks on a newsfeed item, a separate activity (a {@link DetailedViewActivity}) is launched
 * to show the news.
 *
 * In dual-pane mode, this activity shows both {@link NewsFeedFragment} and {@link DetailedViewFragment}.
 * When the user selects a news item, the selected news is shown.
 *
 * User also can swipe across {@link NewsFeedFragment} to refresh news feed data. Also it can be done using
 * "Refresh" button in actionbar.
 */
public class MainActivity extends Activity implements NewsFeedFragment.OnNewsItemSelectedListener {

    // Activity toolbar
    private Toolbar mToolbar;

    // We need to know is activity in Dual-pane mode, or not.
    private boolean mIsDualMode = false;

    // The fragment that shows news feed
    private NewsFeedFragment mNewsFeedFragment;

    // The fragment that shows news itself
    private DetailedViewFragment mDetailedViewFragment;

    private int mSelectedIndex = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);

        //Initialize the toolbar.
        initToolbar();

        // Check is there a Dual-pane mode or not
        mIsDualMode = getResources().getBoolean(R.bool.has_two_panes);

        // Locate News feed fragment
        mNewsFeedFragment = (NewsFeedFragment) getFragmentManager().findFragmentById(R.id.news_feed_fragment_holder);
        if (mNewsFeedFragment == null) {
            mNewsFeedFragment = NewsFeedFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.news_feed_fragment_holder, mNewsFeedFragment)
                    .commit();
        }

        // Locate news view fragment if we have dual-pane mode
        if (mIsDualMode) {
            mDetailedViewFragment = (DetailedViewFragment) getFragmentManager()
                    .findFragmentById(R.id.news_view_fragment_holder);
            if (mDetailedViewFragment == null){
                mDetailedViewFragment = DetailedViewFragment.newInstance();
                getFragmentManager().beginTransaction().replace(R.id.news_view_fragment_holder, mDetailedViewFragment)
                        .commit();
            }
        }

        // Set listener to react on news item selection
        mNewsFeedFragment.setOnNewsItemSelectedListener(this);

        //Set selectable mode for news feed list item depends on current activity mode
        mNewsFeedFragment.setSelectable(mIsDualMode);

        // Restore selected item
        restoreSelection(savedInstanceState);
    }

    /** Restore news selection from saved state. */
    void restoreSelection(Bundle savedInstanceState) {
        if (savedInstanceState != null && mIsDualMode) {
            int itemIndex = savedInstanceState.getInt(Consts.INDEX_STATE_NAME, 0);
            mNewsFeedFragment.setSelectedItem(itemIndex);
        }
    }

    /** Initalizes the toolbar. */
    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
    }

    /** Save instance state. Saves current selected news and news feed list state. */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Consts.FEED_LIST_STATE_NAME, mNewsFeedFragment.getListState());
        outState.putInt(Consts.INDEX_STATE_NAME, mSelectedIndex);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        restoreSelection(savedInstanceState);
        mNewsFeedFragment.setListState(savedInstanceState.getParcelable(Consts.FEED_LIST_STATE_NAME));
    }

    /** Called when a newsItem is selected by {@link NewsFeedFragment} to notify that some news item selected.
     *
     * If we in one-pane mode we send intent to separete activity with news url.
     * If we in double-pane mode, we call {@link DetailedViewFragment} method to display news.
     *
     * @param url the url of selecter news.
     * @param position the index of selected item.
     */
    @Override
    public void onNewsItemSelected(String url, int position) {
        mSelectedIndex = position;
        if (mIsDualMode) {
            mDetailedViewFragment.displayNews(url);
        } else {
            Intent i = new Intent(this, DetailedViewActivity.class);
            i.putExtra(Consts.INTENT_URL_EXTRA, url);
            startActivity(i);
        }

    }
}
