package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represent Pagination item JSON object from news API
 */
public class Pagination {

    @SerializedName("TotalPages")
    private int mTotalPages;

    @SerializedName("PageNo")
    private int mPageNo;

    @SerializedName("PerPage")
    private int mPerPage;

    @SerializedName("WebUrl")
    private String mWebUrl;


    public int getTotalPages() {
        return mTotalPages;
    }

    public int getPageNo() {
        return mPageNo;
    }

    public int getPerPage() {
        return mPerPage;
    }

    public String getWebUrl() {
        return mWebUrl;
    }
}
