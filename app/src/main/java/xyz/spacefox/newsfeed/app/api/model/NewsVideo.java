package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represent Video item JSON object from news API
 */
public class NewsVideo {

    @SerializedName("Thumb")
    private String mThumbUrl;

    @SerializedName("VideoCaption")
    private String mCaption;

    @SerializedName("DetailFeed")
    private String mDetailFeedUrl;

    @SerializedName("Type")
    private String mType;


    public String getThumbUrl() {
        return mThumbUrl;
    }

    public String getCaption() {
        return mCaption;
    }

    public String getDetailFeedUrl() {
        return mDetailFeedUrl;
    }

    public String getType() {
        return mType;
    }

    @Override
    public int hashCode() {
        String toComp = mThumbUrl + mDetailFeedUrl;
        return toComp.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        NewsVideo other = (NewsVideo) o;
        if (!mThumbUrl.equals(other.getThumbUrl()) || !mDetailFeedUrl.equals(other.getDetailFeedUrl()))
            return false;
        return true;
    }
}
