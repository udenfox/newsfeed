package xyz.spacefox.newsfeed.app;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Fragment that displays news.
 *
 * This Fragment displays news in WebView loaded from source website.
 */
public class DetailedViewFragment extends Fragment {

    // Webview to display news
    private WebView mNewsView;

    // Progressbar to show when page is loading
    private ProgressBar mLoadingBar;

    // Textview with "No news selected" sign
    private TextView mNoPageView;

    // Url of news to display
    private String mNewsUrl = null;

    /**
     * Create a new instance of DetailedViewFragment.
     */
    public static DetailedViewFragment newInstance() {
        DetailedViewFragment f = new DetailedViewFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detailed_view, container, false);

        // Init view
        mNewsView = (WebView) v.findViewById(R.id.wv_news_detailed);
        mLoadingBar = (ProgressBar) v.findViewById(R.id.pb_loading_bar);
        mNoPageView = (TextView) v.findViewById(R.id.tv_no_page);

        // Configure WebView to use JavaScript
        WebSettings webSettings = mNewsView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Assigning WebClient to a WebView
        mNewsView.setWebViewClient(new NfWebViewClient());

        if (savedInstanceState == null) {
            loadWebPage();
        }
        else {
            mNewsView.restoreState(savedInstanceState);
            mNoPageView.setVisibility(View.GONE);
        }

        return v;
    }

    /**
     * Displays the news in WebView
     *
     * @param newsUrl Url to displayable news.
     */
    public void displayNews(String newsUrl){
        this.mNewsUrl = newsUrl;
        loadWebPage();
    }

    /**
     * Loads the webpage to a WebView
     */
    private void loadWebPage() {
        if (mNewsView != null && mNewsUrl != null) {
            mNoPageView.setVisibility(View.GONE);
            mNewsView.loadUrl(mNewsUrl);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mNewsView.saveState(outState);
    }

    /**
     * Sub-class of {@link android.webkit.WebViewClient} for use in that fragment.
     * Class overrides methods to implement progress bar showing on page loading starts
     * and hide it when page loaded.
     */
    public class NfWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mLoadingBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mLoadingBar.setVisibility(View.GONE);
        }
    }

}
