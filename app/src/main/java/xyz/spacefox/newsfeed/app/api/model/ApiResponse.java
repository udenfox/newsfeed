package xyz.spacefox.newsfeed.app.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Class that represents JSON API response.
 */
public class ApiResponse {

    @SerializedName("Pagination")
    private Pagination mPagination;

    @SerializedName("NewsItem")
    private List<NewsItem> mNewsItems;


    public Pagination getPagination() {
        return mPagination;
    }

    public List<NewsItem> getNewsItems() {
        return mNewsItems;
    }
}
