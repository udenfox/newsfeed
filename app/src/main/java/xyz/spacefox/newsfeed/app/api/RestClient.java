package xyz.spacefox.newsfeed.app.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import xyz.spacefox.newsfeed.app.api.model.NewsHomeRelated;
import xyz.spacefox.newsfeed.app.api.model.NewsVideo;
import xyz.spacefox.newsfeed.app.utils.Consts;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

/**
 * Singletone class that represents API client using Retrofit.
 * Containt GSON configuration.
 */
public class RestClient {
    private static RestClient sInstance = new RestClient();

    private ApiService mApiService;
    private Gson mGson;
    private RestAdapter mRestAdapter;

    private RestClient() {

        Type mNewsVideoListType = new TypeToken<List<NewsVideo>>() {}.getType();
        Type mNewsHomeRelatedListType = new TypeToken<List<NewsHomeRelated>>() {}.getType();

        mGson = new GsonBuilder()
                .setDateFormat("MMM dd, yyyy, hh.mmaa zzz")
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(mNewsVideoListType, new VideoDeserializer())
                .registerTypeAdapter(mNewsHomeRelatedListType, new HomeRelaterDeserializer())
                .create();

        //TODO: Set log level to non-verbose afrer release
        mRestAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Consts.API_URL)
                .setConverter(new GsonConverter(mGson))
                .build();

        mApiService = mRestAdapter.create(ApiService.class);
    }

    /** Gives client instance */
    public static RestClient getInstance() {
        return sInstance;
    }

    /** Gives api service to work with */
    public ApiService getApiService()
    {
        return mApiService;
    }

    /** Gives GSON object from Rest client */
    public Gson getGson() {
        return mGson;
    }
}
