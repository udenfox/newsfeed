package xyz.spacefox.newsfeed.app.api;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import xyz.spacefox.newsfeed.app.api.model.NewsVideo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Deserializer class for GSON to properly deserialize video JSON object given from API.
 * Checks if video object from JSON is array or single object and returns
 * {@link xyz.spacefox.newsfeed.app.api.model.NewsVideo NewsVideo} objects list in anyway.
 */
public class VideoDeserializer implements JsonDeserializer<List<NewsVideo>>{

    @Override
    public List<NewsVideo> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<NewsVideo> newsVideos = new ArrayList<NewsVideo>();

        if (json.isJsonArray()) {
            for (JsonElement e : json.getAsJsonArray()) {
                newsVideos.add((NewsVideo) context.deserialize(e, NewsVideo.class));
            }
        } else if (json.isJsonObject()) {
            newsVideos.add((NewsVideo) context.deserialize(json, NewsVideo.class));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + json.getClass());
        }

        return newsVideos;
    }
}
