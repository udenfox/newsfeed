# ABOUT APP #
Application displays news feed from the IndianTimes website using the [IndianTimes REST API](http://timesofindia.indiatimes.com/feeds/newsdefaultfeeds.cms?feedtype=sjson).
User can read articles from the newsfeed. Detailed news shown using mobile version of the IndianTimes website through a WebView.

# Key features #
* Interaction with REST API using Retrofit + GSON
* Data stored locally in a SQLite database
* Newsfeed list has Swipe to refresh functionality
* Adaptive layouts: main activity can work in two modes depends on a screen specifications — single pane or double pane.
* Image loading and cache using Picasso.

[**Download APK**](https://bitbucket.org/udenfox/newsfeed/raw/3aa39b63f77cfd7c7d6cf16cc285cbbe962a6cb6/app/NewsFeed-release.apk)